#!/usr/bin/python
import StringIO
import base64
import colorsys
import math
import time
import sys

#import PIL.Image
#import PIL.ImageDraw

import numpy as np

import sdl2 
import sdl2.ext

#import boost

import xklatin1
import tojs

from websocketio import *


wsio = None
img = None
draw = None

totalWidth = 0
totalHeight = 0
appId = ""
start = 0

address = "ws://10.234.2.22:9280"
#address = "ws://localhost:5951"
if len(sys.argv) > 1:
	address = str(sys.argv[1])

def main():
	global wsio
	global img
	global draw
	global start
	
	wsio = WebSocketIO(address)
	
	start = time.time()
	
	wsio.open(on_open) #starts in new thread, and waits indefinitely to listen
        print 'wsio.open after'


x=0
y=0
pressed = {}

def isPressed(key):
  return (key in pressed) and pressed[key]

def shifted(code):
        # US keyboard layout...
        if isPressed(sdl2.SDLK_LSHIFT):
          if code>=xklatin1.XK_a and code<=xklatin1.XK_b:
            return code - 32;
          if code==xklatin1.XK_1: return xklatin1.XK_exclam
          if code==xklatin1.XK_2: return xklatin1.XK_at
          if code==xklatin1.XK_3: return 35
          if code==xklatin1.XK_4: return xklatin1.XK_dollar
          if code==xklatin1.XK_5: return xklatin1.XK_percent
          if code==xklatin1.XK_6: return xklatin1.XK_caret
          if code==xklatin1.XK_7: return xklatin1.XK_ampersand
          if code==xklatin1.XK_8: return xklatin1.XK_asterisk
          if code==xklatin1.XK_9: return xklatin1.XK_parenleft
          if code==xklatin1.XK_0: return xklatin1.XK_parenright
          if code==xklatin1.XK_minus: return xklatin1.XK_underscore
          if code==xklatin1.XK_equal: return xklatin1.XK_plus
          if code==xklatin1.XK_backslash: return 124
          if code==xklatin1.XK_bracketleft: return xklatin1.XK_braceleft
          if code==xklatin1.XK_bracketright: return xklatin1.XK_braceleft
          if code==xklatin1.XK_semicolon: return xklatin1.XK_colon
          if code==xklatin1.XK_quoteright: return xklatin1.XK_quotedbl
          if code==xklatin1.XK_comma: return xklatin1.XK_less
          if code==xklatin1.XK_period: return xklatin1.XK_greater
          if code==xklatin1.XK_slash: return xklatin1.XK_question
	return code

def on_open():
        global pressed
	global wsio

	wsio.on('initialize', initialize)
	wsio.on('setupDisplayConfiguration', setupDisplayConfiguration)
	
	wsio.emit('addClient', {'clientType': "sageUI", 'requests': {'config': True, 'version': False, 'time': False, 'console': False}})
	wsio.emit('registerInteractionClient', {'name': "PyPointer", 'color': "#B4B4B4"})
	wsio.emit('startSagePointer', {'label': "PyPointer", 'color': "#B4B4B4"})

        # SDL
        sdl2.ext.init()
	sdl2.SDL_SetRelativeMouseMode(True);
	window = sdl2.ext.Window("sage2pointer", size=(180, 120))
	window.show()
 
        #global x,y

	running = True
        wasScroll = False
        hasFocus = True

	while running:
	    events = sdl2.ext.get_events()
	    for event in events:
                if event.type == sdl2.SDL_WINDOWEVENT and event.window.event == sdl2.SDL_WINDOWEVENT_FOCUS_GAINED:
            	  print("Gained keyboard focus");
                  hasFocus = True
                # pyPointer currently does not have focus - ignore events
                if hasFocus == False:
                  continue
                print 'hasfocus'
                isScroll = False
		if event.type == sdl2.SDL_QUIT:
		    running = False
		    break
		if event.type == sdl2.SDL_MOUSEMOTION:
                    print 'mousemotion'
                    dx = event.motion.xrel
                    dy = event.motion.yrel
                    if (abs(dx)>20 or abs(dy)>20):
                      print 'pointerMove fast '+str(dx)+' '+str(dy)
                      dx=8*dx
                      dy=8*dy
		    data = {
			     'dx': dx,
			     'dy': dy,
			 }
		    wsio.emit("pointerMove", data);
                if (event.type == sdl2.SDL_MOUSEBUTTONDOWN or event.type == sdl2.SDL_MOUSEBUTTONUP) and event.button.button==4:
		    wsio.emit("pointerScrollStart", {});
                    wsio.emit("pointerScroll", {'wheelDelta':-10})
                    isScroll = True
                if (event.type == sdl2.SDL_MOUSEBUTTONDOWN or event.type == sdl2.SDL_MOUSEBUTTONUP) and event.button.button==5:
		    wsio.emit("pointerScrollStart", {});
                    wsio.emit("pointerScroll", {'wheelDelta':10})
                    isScroll = True
                if event.type == sdl2.SDL_MOUSEBUTTONDOWN:
                    s2button = ''
                    print 'button '+str(event.button.button)
                    if event.button.button == sdl2.SDL_BUTTON_LEFT:
                      s2button = 'left'
                    elif event.button.button == sdl2.SDL_BUTTON_MIDDLE:
                      s2button = 'middle'
                    elif event.button.button == sdl2.SDL_BUTTON_RIGHT:
                      s2button = 'right'
                    #elif event.button.button == sdl2.SDL_BUTTON_X1:
                    #  s2button = 'x1'
                    #elif event.button.button == sdl2.SDL_BUTTON_X2:
                    #  s2button = 'x2'
                    if s2button != '':
                      data = {'button': s2button}
		      wsio.emit("pointerPress", data);
                if event.type == sdl2.SDL_MOUSEBUTTONUP:
                    s2button = ''
                    if event.button.button == sdl2.SDL_BUTTON_LEFT:
                      s2button = 'left'
                    elif event.button.button == sdl2.SDL_BUTTON_MIDDLE:
                      s2button = 'middle'
                    elif event.button.button == sdl2.SDL_BUTTON_RIGHT:
                      s2button = 'right'
                    #elif event.button.button == sdl2.SDL_BUTTON_X1:
                    #  s2button = 'x1'
                    #elif event.button.button == sdl2.SDL_BUTTON_X2:
                    #  s2button = 'x2'
                    if s2button != '':
                      data = {'button': s2button}
		      wsio.emit("pointerRelease", data);
                if event.type == sdl2.SDL_MOUSEWHEEL:
                    print 'wheel '+str(event.wheel.x)
		    wsio.emit("pointerScrollStart", {});
                    data = {'wheelDelta': event.wheel.x}
		    wsio.emit("pointerScroll", data);
                    isScroll = True
                if event.type == sdl2.SDL_KEYDOWN or event.type == sdl2.SDL_KEYUP:
                    # record currently-pressed keys; what has changed with this key event?
                    pressed[event.key.keysym.sym] = (event.type == sdl2.SDL_KEYDOWN)
                    print 'pressed = '+str(isPressed(event.key.keysym.sym))
                # ALT-TAB
                if event.type == sdl2.SDL_KEYDOWN and isPressed(sdl2.SDLK_LSHIFT) and isPressed(sdl2.SDLK_LALT) and isPressed(sdl2.SDLK_LCTRL) and isPressed(sdl2.SDLK_t):
                      print 'alt-tab'
                      data = {'code': event.key.keysym.sym}
                      wsio.emit("keyDown", {'code': 18});
                      wsio.emit("keyDown", {'code': 9});
                      wsio.emit("keyUp", {'code': 9});
                      wsio.emit("keyUp", {'code': 18});
                      continue
                if event.type == sdl2.SDL_KEYDOWN or event.type == sdl2.SDL_KEYUP:
		    print("keyEvent down==", str(event.type == sdl2.SDL_KEYDOWN));
		    print("keyEvent ", event.key.keysym.scancode);
		    print("keyEvent ", event.key.keysym.sym);
                    comboCode = None
                    if event.key.keysym.scancode in tojs.to_scan:
		      scancode = tojs.to_scan[event.key.keysym.scancode];
                    else:
                      print 'UNKOWN SCAN CODE'
                      continue
                    char = None
		    if event.key.keysym.sym in tojs.to_char:
		      print("keyEvent ", unichr(event.key.keysym.sym));
		      uschar = tojs.to_char[event.key.keysym.sym];
		      char = shifted(uschar);
		      print 'char (unshifted) '+str(uschar)
		      print 'char (possibly-shifted) '+str(char)
                    # send event/s
                    if event.type == sdl2.SDL_KEYDOWN:
                      data = {'code': scancode}
                      print 'keyDown '+str(data)
                      wsio.emit("keyDown", data);
                      if char != None:
                        data = {'code': char, 'character': unichr(char)}
                        print 'keyPress '+str(data)
                        wsio.emit("keyPress", data);
                    else:
                      data = {'code': scancode}
                      print 'keyUp '+str(data)
                      wsio.emit("keyUp", data);
                if event.type == sdl2.SDL_WINDOWEVENT and event.window.event == sdl2.SDL_WINDOWEVENT_FOCUS_LOST:
            	  print("Lost keyboard focus");
                  hasFocus = False
                if wasScroll and not isScroll:
                    wsio.emit("pointerScrollEnd", {})
                wasScroll = isScroll
	    window.refresh()

        wsio.emit('stopSagePointer',{})
        print 'close'
        wsio.ws.close()
	exit(0)

def initialize(data):
	print "initialize"
	global wsio
	global appId
	global img
	
	appId = data['UID']
	
	width, height = img.size
	jpeg = captureFrame()
	
	##wsio.emit('startSagePointer', {label: "PyPointer", color: "#B4B4B4"})
	print "started app"

def setupDisplayConfiguration(data):
	global totalWidth
	global totalHeight
	
	totalWidth = data['totalWidth']
	totalHeight = data['totalHeight']
	print "SAGE wall: " + str(totalWidth) + "x" + str(totalHeight)

#def requestNextFrame(data):
#	global appId
#	
#	jpeg = captureFrame()
#	
#	wsio.emit('updateMediaStreamFrame', {'id': appId+"|0", 'state': {'src': jpeg, 'type': "image/jpeg", 'encoding': "base64"}})
	
main()
